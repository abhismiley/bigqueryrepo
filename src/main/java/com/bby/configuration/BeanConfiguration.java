package com.bby.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bby.authorization.BigQueryAuthorization;

@Configuration
public class BeanConfiguration {

	
	@Bean
	public static BigQueryAuthorization getPollJdbcServiceImpl() {
		return new BigQueryAuthorization();
	}
}
